﻿Imports System.Data.SqlServerCe
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient

Public Class Form1
    Dim cn As SqlConnection
    Public distributor As String = ""
    Public unit As String = ""
    Public produk As String = ""
    Public nama_file As String = ""
    Public code_cust_list As String = ""
    Public cust_name_list As String = ""
    Dim StrConn As String
    Public Sub ConnectToSQLServer()
        Try
            StrConn = "Data Source= WIN-COG4TSLIIGA\SERVERSKG;Initial Catalog= dtSKGServer ;User ID= sa;Password= Mis2003;MultipleActiveResultSets=true"
            'StrConn = "Data Source= localhost;Initial Catalog= dtSKGServer ;User ID= sa;Password= sinde1#;MultipleActiveResultSets=true"
          
            cn = New SqlConnection(StrConn)
            If cn.State = ConnectionState.Open Then
                cn.Close()
            Else
                cn = New SqlConnection(StrConn)
                cn.Open()

            End If



        Catch ex As Exception
            MsgBox("[ " & Err.Number.ToString & "  ] " & ex.Message, MsgBoxStyle.Critical, "Error Contact your Administrator")
            cn.Dispose()
        End Try
    End Sub


    'Private Sub MyConnection()
    '    Try
    '        Dim cnSQL As String = "Data Source=" & Application.StartupPath & "\Database1.sdf;Persist Security Info=False"
    '        Me.cn = New SqlCeConnection(cnSQL)
    '        If ConnectionState.Closed Then
    '            Me.cn.Open()
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DataGridView1.Hide()
        DataGridView2.Hide()
        setHeaderDGV()
        Label1.Hide()
        cbx_cust.Hide()
        btn_pilihCust.Hide()
        setHeaderDGVListFile()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_upload.Click
        Try
            btn_upload.Enabled = False
            Label1.Hide()
            cbx_cust.Hide()
            btn_pilihCust.Hide()

            importExcelTargetUnit()
            btn_upload.Enabled = True
        Catch ex As Exception
            MsgBox("ERROR Upload data = " + ex.Message, MsgBoxStyle.Critical, "ERROR")
            btn_upload.Enabled = True
        End Try
    End Sub

    Public Sub importExcelTargetUnit()
        Dim conn As OleDbConnection
        Dim dta As New OleDbDataAdapter
        Dim dts As New DataSet
        Dim excel As String
        Dim OpenFileDialog As New OpenFileDialog
        Dim dt As New DataTable
        Dim fi As FileInfo
        Dim FileName As String
        DataGridView1.Hide()

        OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        OpenFileDialog.Filter = "All Files (*.*)|*.*|Excel files (*.xlsx)|*.xlsx|CSV Files (*.csv)|*.csv|XLS Files (*.xls)|*xls"

        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Try
                fi = New FileInfo(OpenFileDialog.FileName)
                FileName = OpenFileDialog.FileName

                Label2.Text = fi.Name
                nama_file = fi.Name
                Label3.Text = fi.FullName

                excel = fi.FullName
                conn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excel + ";Extended Properties='Excel 12.0;HDR:NO;IMEX=1';")
                dta = New OleDbDataAdapter("Select * From [TARGET UNIT$]", conn)
                dts = New DataSet
                dta.Fill(dts, "[TARGET UNIT$]")
                dt = dts.Tables("[TARGET UNIT$]")
                Dim Fuldistributor = dt.Rows(1).ItemArray(2).ToString
                If Fuldistributor.Contains(",") Or Fuldistributor.Contains(".") Then
                    distributor = Fuldistributor.Substring(0, Fuldistributor.IndexOf(",")).Trim

                Else
                    distributor = Fuldistributor.Trim

                End If
                unit = dt.Rows(2).ItemArray(2).ToString
                'For i As Integer = 0 To dt.Rows.Count - 1
                '    If dt.Rows(i).ItemArray(0).ToString().Contains("BADAK") Then
                '        produk = "BADAK"
                '    End If
                '    If dt.Rows(i).ItemArray(0).ToString().Contains("LSG") Or dt.Rows(1).ItemArray(0).ToString().Contains("LASEGAR") Then
                '        produk = "LSG"
                '    End If
                'Next
                For i As Integer = 0 To 4
                    dt.Rows(i).Delete()
                Next
                DataGridView2.DataSource = Nothing
                DataGridView2.DataSource = dt
                For Each row As DataGridViewRow In DataGridView2.Rows
                    row.Cells(1).Value = distributor
                Next
                'DataGridView2.DataSource = DataGridView1.DataSource
                For i As Integer = 0 To DataGridView2.ColumnCount - 1
                    DataGridView2.Columns(i).HeaderText = DataGridView2.Rows(0).Cells(i).Value.ToString
                Next

                DataGridView2.Columns(1).HeaderText = "DISTRIBUTOR"
                DataGridView2.AllowUserToAddRows = False
                DataGridView2.AllowUserToDeleteRows = True
                DataGridView2.Rows.Remove(DataGridView2.Rows(0))
                conn.Close()
            Catch ex As Exception
                MsgBox("Error " + ex.Message, MsgBoxStyle.Information, "ERROR")
            End Try
            'mappingToTGCGrid(dgv_tgc, DataGridView2, Nothing)
            getDataCustToComboBox(DataGridView2.Rows(0).Cells(1).Value.ToString, Nothing)
        End If
    End Sub

    Public Function getDataPrice(ByVal custCode As String, ByVal itemCode As String) As DataTable
        ConnectToSQLServer()

        Dim dtSet As DataSet = New DataSet
        Dim dtSetArea As DataSet = New DataSet
        Dim dt As New DataTable
        Dim dtArea As New DataTable
        Dim dtAdapter As New SqlDataAdapter

        dtAdapter = New SqlDataAdapter("SELECT * FROM detail2_product0120 AS a INNER JOIN master_customer0120 AS b ON a.zone_code = b.zone_code WHERE (a.item_code = '" + itemCode + "') AND (b.cust_code = '" + custCode + "')", cn)

        dt = Nothing
        dtAdapter.Fill(dtSet)
        dt = dtSet.Tables(0)
        getDataPrice = dt
        cn.Close()

    End Function

    Public Function getDataCustToComboBox(ByVal custName As String, ByVal cust_code As String) As DataTable
        ConnectToSQLServer()

        Dim dtSet As DataSet = New DataSet
        Dim dtSetArea As DataSet = New DataSet
        Dim dt As New DataTable
        Dim dtArea As New DataTable
        Dim dtAdapter As New SqlDataAdapter

        If cust_code = Nothing Then
            dtAdapter = New SqlDataAdapter("Select * from master_customer0120 where cust_name like '%" + custName + "%'", cn)
        ElseIf cust_code IsNot Nothing Then
            dtAdapter = New SqlDataAdapter("Select * from master_customer0120 where cust_code like '" + cust_code + "'", cn)
        End If
        dt = Nothing
        dtAdapter.Fill(dtSet)
        dt = dtSet.Tables(0)
        Label1.Show()
        btn_pilihCust.Show()
        cbx_cust.Show()
        cbx_cust.Items.Clear()
        If cust_code IsNot Nothing Then
            cust_name_list = dt.Rows(0).ItemArray(7).ToString
        End If
        For I As Integer = 0 To dt.Rows.Count - 1
            cbx_cust.DisplayMember = "Value"
            cbx_cust.ValueMember = "Key"
            Dim display As String = dt.Rows(I).ItemArray(0).ToString + " -- " + dt.Rows(I).ItemArray(7).ToString
            Dim key As String = dt.Rows(I).ItemArray(0).ToString
            cbx_cust.Text = Nothing
            cbx_cust.Items.Add(New DictionaryEntry(key, display))
        Next
        getDataCustToComboBox = dt
        cn.Close()

    End Function

    Public Function getDataProductFromSDF(ByVal productName As String) As DataTable
        ConnectToSQLServer()

        Dim dtSet As DataSet = New DataSet
        Dim dtAdapter As SqlDataAdapter = New SqlDataAdapter("Select a.* from master_product a join product_code_name b on a.item_code = b.pcode where b.pname like '%" + productName + "%'", cn)
        dtAdapter.Fill(dtSet)
        getDataProductFromSDF = dtSet.Tables(0)
        cn.Close()

    End Function
    Public Sub setHeaderDGV()
        dgv_tgc.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        dgv_tgc.Columns.Add("ITEM_CODE", "ITEM_CODE")
        dgv_tgc.Columns.Add("CUST_CODE", "CUST_CODE")
        dgv_tgc.Columns.Add("CUST_NAME", "CUST_NAME")
        dgv_tgc.Columns.Add("ITEM_NAME", "ITEM_NAME")
        dgv_tgc.Columns.Add("CUST_CONTR", "CUST_CONTR")
        dgv_tgc.Columns.Add("CUST_NICK", "CUST_NICK")
        dgv_tgc.Columns.Add("CUST_RAY", "CUST_RAY")
        dgv_tgc.Columns.Add("CUST_AREA", "CUST_AREA")
        dgv_tgc.Columns.Add("CUST_WIL", "CUST_WIL")
        dgv_tgc.Columns.Add("ZONE_CODE", "ZONE_CODE")
        dgv_tgc.Columns.Add("ITEM_GRUP", "ITEM_GRUP")
        dgv_tgc.Columns.Add("DEPT_CODE", "DEPT_CODE")
        dgv_tgc.Columns.Add("CUST_MAST", "CUST_MAST")
        dgv_tgc.Columns.Add("CUST_STAT", "CUST_STAT")
        dgv_tgc.Columns.Add("TAHUN", "TAHUN")
        dgv_tgc.Columns.Add("CUST_ITEM", "CUST_ITEM")
        dgv_tgc.Columns.Add("T_VALUE", "T_VALUE")
        dgv_tgc.Columns.Add("TOTAL", "TOTAL")
        dgv_tgc.Columns.Add("RP", "RP")
        dgv_tgc.Columns.Add("HARGA", "HARGA")
        dgv_tgc.Columns.Add("S01", "S01")
        dgv_tgc.Columns.Add("S02", "S02")
        dgv_tgc.Columns.Add("S03", "S03")
        dgv_tgc.Columns.Add("S04", "S04")
        dgv_tgc.Columns.Add("S05", "S05")
        dgv_tgc.Columns.Add("S06", "S06")
        dgv_tgc.Columns.Add("S07", "S07")
        dgv_tgc.Columns.Add("S08", "S08")
        dgv_tgc.Columns.Add("S09", "S09")
        dgv_tgc.Columns.Add("S10", "S10")
        dgv_tgc.Columns.Add("S11", "S11")
        dgv_tgc.Columns.Add("S12", "S12")
        dgv_tgc.Columns.Add("UOC_1", "UOC_1")
        dgv_tgc.Columns.Add("UOC_2", "UOC_2")
        dgv_tgc.Columns.Add("UOC_3", "UOC_3")
        dgv_tgc.Columns.Add("UOM_1", "UOM_1")
        dgv_tgc.Columns.Add("UOM_2", "UOM_2")
        dgv_tgc.Columns.Add("UOM_3", "UOM_3")
        dgv_tgc.Columns.Add("SISA", "SISA")
        dgv_tgc.Columns.Add("SLSM_CODE", "SLSM_CODE")
        dgv_tgc.Columns.Add("RM_CODE", "RM_CODE")
        dgv_tgc.Columns.Add("SM_CODE", "SM_CODE")
        dgv_tgc.Columns.Add("SMT01", "SMT01")
        dgv_tgc.Columns.Add("SMT02", "SMT02")
        dgv_tgc.Columns.Add("SMT03", "SMT03")
        dgv_tgc.Columns.Add("SMT04", "SMT04")
        dgv_tgc.Columns.Add("SMT05", "SMT05")
        dgv_tgc.Columns.Add("SMT06", "SMT06")
        dgv_tgc.Columns.Add("SMT07", "SMT07")
        dgv_tgc.Columns.Add("SMT08", "SMT08")
        dgv_tgc.Columns.Add("SMT09", "SMT09")
        dgv_tgc.Columns.Add("SMT10", "SMT10")
        dgv_tgc.Columns.Add("SMT11", "SMT11")
        dgv_tgc.Columns.Add("SMT12", "SMT12")
    End Sub

    Public Sub mappingToTGCGrid(ByVal tgcGridInput As DataGridView, ByVal gridTargetUnit As DataGridView, ByVal code_cust As String)
        'Try
        Dim tgcGrid As DataGridView = tgcGridInput
        Dim S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11, S12 As New Decimal
        Dim UOC1, UOC2, UOC3, UOM1, UOM2, UOM3 As New Object
        Dim dtCust As New DataTable
        Dim dtProduct As New DataTable
        Dim dtPrice As New DataTable
        Dim item_code As Object = Nothing
        Dim total As Integer = 0
        Dim rp As Integer = 0
        Dim harga As Integer = 0
        If code_cust Is Nothing Or code_cust = "" Then
            dtCust = getDataCustToComboBox(gridTargetUnit.Rows(0).Cells(1).Value.ToString, Nothing)

        Else
            dtCust = getDataCustToComboBox(Nothing, code_cust)
            Dim countA As Integer = 0
            Dim tgcCount As Integer = 0
            tgcCount = dgv_tgc.Rows.Count - 1
            For i As Integer = 0 To gridTargetUnit.Rows.Count - 2
                If Not IsDBNull(gridTargetUnit.Rows(i).Cells(0).Value) Then
                    countA += 1
                End If
                If gridTargetUnit.Rows(i).Cells(0).Value.ToString = "Grand Total" Then
                    countA -= 1
                End If
            Next
            For i As Integer = 0 To countA - 1
                Dim j As Integer = tgcCount
                j += i
                If getDataProductFromSDF(gridTargetUnit.Rows(i).Cells(0).Value.ToString) IsNot Nothing Then
                    tgcGrid.Rows.Add()
                    dtProduct = getDataProductFromSDF(gridTargetUnit.Rows(i).Cells(0).Value.ToString)
                    If dtProduct.Rows.Count > 0 Then
                        item_code = dtProduct.Rows(0).ItemArray(3)
                        total = 0
                        S01 = If(gridTargetUnit.Rows(i).Cells(2).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(2).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(2).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S02 = If(gridTargetUnit.Rows(i).Cells(3).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(3).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(3).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S03 = If(gridTargetUnit.Rows(i).Cells(4).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(4).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(4).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S04 = If(gridTargetUnit.Rows(i).Cells(5).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(5).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(5).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S05 = If(gridTargetUnit.Rows(i).Cells(6).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(6).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(6).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S06 = If(gridTargetUnit.Rows(i).Cells(7).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(7).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(7).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S07 = If(gridTargetUnit.Rows(i).Cells(8).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(8).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(8).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S08 = If(gridTargetUnit.Rows(i).Cells(9).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(9).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(9).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S09 = If(gridTargetUnit.Rows(i).Cells(10).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(10).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(10).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S10 = If(gridTargetUnit.Rows(i).Cells(11).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(11).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(11).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S11 = If(gridTargetUnit.Rows(i).Cells(12).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(12).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(12).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S12 = If(gridTargetUnit.Rows(i).Cells(13).Value.ToString = "-" Or gridTargetUnit.Rows(i).Cells(13).Value.ToString = "", 0, CDec(CDec(gridTargetUnit.Rows(i).Cells(13).Value) * dtProduct.Rows(0).ItemArray(14) * dtProduct.Rows(0).ItemArray(15)))
                        S01 = Math.Ceiling(S01)
                        S02 = Math.Ceiling(S02)
                        S03 = Math.Ceiling(S03)
                        S04 = Math.Ceiling(S04)
                        S05 = Math.Ceiling(S05)
                        S06 = Math.Ceiling(S06)
                        S07 = Math.Ceiling(S07)
                        S08 = Math.Ceiling(S08)
                        S09 = Math.Ceiling(S09)
                        S10 = Math.Ceiling(S10)
                        S11 = Math.Ceiling(S11)
                        S12 = Math.Ceiling(S12)
                        UOC1 = dtProduct.Rows(0).ItemArray(10)
                        UOC2 = dtProduct.Rows(0).ItemArray(11)
                        UOC3 = dtProduct.Rows(0).ItemArray(12)
                        UOM1 = dtProduct.Rows(0).ItemArray(13)
                        UOM2 = dtProduct.Rows(0).ItemArray(14)
                        UOM3 = dtProduct.Rows(0).ItemArray(15)
                        total = S01 + S02 + S03 + S04 + S05 + S06 + S07 + S08 + S09 + S10 + S11 + S12
                    ElseIf dtProduct.Rows.Count = 0 Or dtProduct.Rows.Count < 1 Then
                        item_code = "-"
                        total = 0
                        S01 = 0
                        S02 = 0
                        S03 = 0
                        S04 = 0
                        S05 = 0
                        S06 = 0
                        S07 = 0
                        S08 = 0
                        S09 = 0
                        S10 = 0
                        S11 = 0
                        S12 = 0
                        UOC1 = "-"
                        UOC2 = "-"
                        UOC3 = "-"
                        UOM1 = 0
                        UOM2 = 0
                        UOM3 = 0
                    End If
                    dtPrice = getDataPrice(code_cust, item_code)
                    If dtPrice.Rows.Count > 0 Then
                        rp = dtPrice.Rows(0).ItemArray(7)
                        harga = dtPrice.Rows(0).ItemArray(7)
                    ElseIf dtPrice.Rows.Count = 0 Or dtPrice.Rows.Count < 1 Then
                        rp = 0
                        harga = 0
                    End If
                End If

                tgcGrid.Rows(j).Cells(0).Value = item_code
                tgcGrid.Rows(j).Cells(1).Value = dtCust.Rows(0).ItemArray(0)
                tgcGrid.Rows(j).Cells(2).Value = dtCust.Rows(0).ItemArray(7)
                tgcGrid.Rows(j).Cells(3).Value = gridTargetUnit.Rows(i).Cells(0).Value
                tgcGrid.Rows(j).Cells(4).Value = "-"
                tgcGrid.Rows(j).Cells(5).Value = dtCust.Rows(0).ItemArray(8)
                tgcGrid.Rows(j).Cells(6).Value = If(dtCust.Rows.Count > 0, dtCust.Rows(0).ItemArray(3), "-")
                tgcGrid.Rows(j).Cells(7).Value = If(dtCust.Rows.Count > 0, dtCust.Rows(0).ItemArray(4), "-")
                tgcGrid.Rows(j).Cells(8).Value = If(dtCust.Rows.Count > 0, dtCust.Rows(0).ItemArray(5), "-")
                tgcGrid.Rows(j).Cells(9).Value = If(dtProduct.Rows.Count > 0, dtCust.Rows(0).ItemArray(6), "-")
                tgcGrid.Rows(j).Cells(10).Value = If(dtProduct.Rows.Count > 0, dtProduct.Rows(0).ItemArray(7), "-")
                tgcGrid.Rows(j).Cells(11).Value = If(dtProduct.Rows.Count > 0, dtProduct.Rows(0).ItemArray(8), "-")
                tgcGrid.Rows(j).Cells(12).Value = "-"
                tgcGrid.Rows(j).Cells(13).Value = If(dtCust.Rows(0).ItemArray(27) = 1, True, False)
                tgcGrid.Rows(j).Cells(14).Value = Convert.ToInt32(Now.ToString("yyyy"))
                tgcGrid.Rows(j).Cells(15).Value = tgcGrid.Rows(j).Cells(1).Value + tgcGrid.Rows(j).Cells(0).Value
                tgcGrid.Rows(j).Cells(16).Value = 0
                tgcGrid.Rows(j).Cells(17).Value = total
                tgcGrid.Rows(j).Cells(18).Value = rp
                tgcGrid.Rows(j).Cells(19).Value = harga
                tgcGrid.Rows(j).Cells(20).Value = S01
                tgcGrid.Rows(j).Cells(21).Value = S02
                tgcGrid.Rows(j).Cells(22).Value = S03
                tgcGrid.Rows(j).Cells(23).Value = S04
                tgcGrid.Rows(j).Cells(24).Value = S05
                tgcGrid.Rows(j).Cells(25).Value = S06
                tgcGrid.Rows(j).Cells(26).Value = S07
                tgcGrid.Rows(j).Cells(27).Value = S08
                tgcGrid.Rows(j).Cells(28).Value = S09
                tgcGrid.Rows(j).Cells(29).Value = S10
                tgcGrid.Rows(j).Cells(30).Value = S11
                tgcGrid.Rows(j).Cells(31).Value = S12
                tgcGrid.Rows(j).Cells(32).Value = UOC1
                tgcGrid.Rows(j).Cells(33).Value = UOC2
                tgcGrid.Rows(j).Cells(34).Value = UOC3
                tgcGrid.Rows(j).Cells(35).Value = UOM1
                tgcGrid.Rows(j).Cells(36).Value = UOM2
                tgcGrid.Rows(j).Cells(37).Value = UOM3
                tgcGrid.Rows(j).Cells(38).Value = 0
                tgcGrid.Rows(j).Cells(39).Value = "-"
                tgcGrid.Rows(j).Cells(40).Value = "-"
                tgcGrid.Rows(j).Cells(41).Value = "-"
                tgcGrid.Rows(j).Cells(42).Value = 0
                tgcGrid.Rows(j).Cells(43).Value = 0
                tgcGrid.Rows(j).Cells(44).Value = 0
                tgcGrid.Rows(j).Cells(45).Value = 0
                tgcGrid.Rows(j).Cells(46).Value = 0
                tgcGrid.Rows(j).Cells(47).Value = 0
                tgcGrid.Rows(j).Cells(48).Value = 0
                tgcGrid.Rows(j).Cells(49).Value = 0
                tgcGrid.Rows(j).Cells(50).Value = 0
                tgcGrid.Rows(j).Cells(51).Value = 0
                tgcGrid.Rows(j).Cells(52).Value = 0
                tgcGrid.Rows(j).Cells(53).Value = 0
            Next
            MsgBox("Data Berhasil diUpload sebanyak " + countA.ToString + " Data", MsgBoxStyle.Information, "INFORMATION")
        End If

        'Catch ex As Exception
        '    MsgBox("ERROR =  " + ex.Message)
        'End Try

    End Sub

    Public Function getDescProduct(ByVal productName As String) As DataTable
        ConnectToSQLServer()


        Dim dtSet As DataSet = New DataSet
        Dim dtAdapter As SqlDataAdapter = New SqlDataAdapter("Select * from mastercustomer where cust_name like '%" + productName + "%'", cn)
        dtAdapter.Fill(dtSet)
        getDescProduct = dtSet.Tables(0)
        cn.Close()

    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btn_pilihCust.Click
        Try
            btn_upload.Enabled = False
            btn_pilihCust.Enabled = False
            Dim cust_code As String = ""
            If String.IsNullOrEmpty(cbx_cust.Text) Then
                MsgBox("Pilih Customer", MsgBoxStyle.Information, "INFORMATION")
            Else
                cust_code = DirectCast(cbx_cust.SelectedItem, DictionaryEntry).Key
                code_cust_list = cust_code
            End If
            mappingToTGCGrid(dgv_tgc, DataGridView2, cust_code)
            setDGVListFile()

            btn_upload.Enabled = True
            btn_pilihCust.Enabled = True
        Catch ex As Exception
            MsgBox("ERROR saat ambil data = " + ex.Message, MsgBoxStyle.Critical, "ERROR")
            btn_upload.Enabled = True
            btn_pilihCust.Enabled = True
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btn_simpan.Click
        saveFromDgv(dgv_tgc)
        cbx_cust.Text = Nothing
        cbx_cust.Hide()
        btn_pilihCust.Hide()
        dgv_tgc.Rows.Clear()

    End Sub

    Private Sub saveFromDgv(ByVal tgcGridInput As DataGridView)
        Dim tgcGrid As New DataGridView
        tgcGrid = tgcGridInput
        cekISUpload(tgcGrid.Rows(0).Cells(1).Value.ToString, tgcGrid.Rows(0).Cells(14).Value.ToString)
        Try
            For i As Integer = 0 To tgcGrid.Rows.Count - 2
                ConnectToSQLServer()
                If Not IsDBNull(tgcGrid.Rows(i).Cells(3).Value) Then
                    Dim query As String = String.Empty
                    query &= "INSERT INTO [TGCM2018] ([ITEM_CODE], [CUST_CODE], [CUST_NAME], [ITEM_NAME], [CUST_CONTR], [CUST_NICK], [CUST_RAY], [CUST_AREA], [CUST_WIL], [ZONE_CODE], [ITEM_GRUP], [DEPT_CODE], [CUST_MAST], [CUST_STAT], [TAHUN], [CUST_ITEM], [T_VALUE], [TOTAL], [RP], [HARGA], [S01], [S02], [S03], [S04], [S05], [S06], [S07], [S08], [S09], [S10], [S11], [S12], [UOC_1], [UOC_2], [UOC_3], [UOM_1], [UOM_2], [UOM_3], [SISA], [SLSM_CODE], [RM_CODE], [SM_CODE], [SMT01], [SMT02], [SMT03], [SMT04], [SMT05], [SMT06], [SMT07], [SMT08], [SMT09], [SMT10], [SMT11], [SMT12]) " +
                             "VALUES (@ITEM_CODE, @CUST_CODE, @CUST_NAME, @ITEM_NAME, @CUST_CONTR, @CUST_NICK, @CUST_RAY, @CUST_AREA, @CUST_WIL, @ZONE_CODE, @ITEM_GRUP, @DEPT_CODE, @CUST_MAST, @CUST_STAT, @TAHUN, @CUST_ITEM, @T_VALUE, @TOTAL, @RP, @HARGA, @S01, @S02, @S03, @S04, @S05, @S06, @S07, @S08, @S09, @S10, @S11, @S12, @UOC_1, @UOC_2, @UOC_3, @UOM_1, @UOM_2, @UOM_3, @SISA, @SLSM_CODE, @RM_CODE, @SM_CODE, @SMT01, @SMT02, @SMT03, @SMT04, @SMT05, @SMT06, @SMT07, @SMT08, @SMT09, @SMT10, @SMT11, @SMT12)"
                    Dim comm As New SqlCommand(query, cn)
                    comm.Parameters.AddWithValue("@ITEM_CODE", tgcGrid.Rows(i).Cells(0).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_CODE", tgcGrid.Rows(i).Cells(1).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_NAME", tgcGrid.Rows(i).Cells(2).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@ITEM_NAME", tgcGrid.Rows(i).Cells(3).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_CONTR", tgcGrid.Rows(i).Cells(4).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_NICK", tgcGrid.Rows(i).Cells(5).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_RAY", tgcGrid.Rows(i).Cells(6).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_AREA", tgcGrid.Rows(i).Cells(7).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_WIL", tgcGrid.Rows(i).Cells(8).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@ZONE_CODE", tgcGrid.Rows(i).Cells(9).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@ITEM_GRUP", tgcGrid.Rows(i).Cells(10).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@DEPT_CODE", tgcGrid.Rows(i).Cells(11).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_MAST", tgcGrid.Rows(i).Cells(12).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_STAT", If((tgcGrid.Rows(i).Cells(13).Value = True), "TRUE", "FALSE"))
                    comm.Parameters.AddWithValue("@TAHUN", tgcGrid.Rows(i).Cells(14).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@CUST_ITEM", tgcGrid.Rows(i).Cells(15).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@T_VALUE", tgcGrid.Rows(i).Cells(16).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@TOTAL", tgcGrid.Rows(i).Cells(17).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@RP", tgcGrid.Rows(i).Cells(18).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@HARGA", tgcGrid.Rows(i).Cells(19).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S01", tgcGrid.Rows(i).Cells(20).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S02", tgcGrid.Rows(i).Cells(21).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S03", tgcGrid.Rows(i).Cells(22).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S04", tgcGrid.Rows(i).Cells(23).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S05", tgcGrid.Rows(i).Cells(24).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S06", tgcGrid.Rows(i).Cells(25).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S07", tgcGrid.Rows(i).Cells(26).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S08", tgcGrid.Rows(i).Cells(27).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S09", tgcGrid.Rows(i).Cells(28).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S10", tgcGrid.Rows(i).Cells(29).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S11", tgcGrid.Rows(i).Cells(30).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@S12", tgcGrid.Rows(i).Cells(31).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@UOC_1", tgcGrid.Rows(i).Cells(32).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@UOC_2", tgcGrid.Rows(i).Cells(33).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@UOC_3", tgcGrid.Rows(i).Cells(34).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@UOM_1", tgcGrid.Rows(i).Cells(35).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@UOM_2", tgcGrid.Rows(i).Cells(36).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@UOM_3", tgcGrid.Rows(i).Cells(37).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SISA", tgcGrid.Rows(i).Cells(38).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SLSM_CODE", tgcGrid.Rows(i).Cells(39).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@RM_CODE", tgcGrid.Rows(i).Cells(40).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SM_CODE", tgcGrid.Rows(i).Cells(41).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT01", tgcGrid.Rows(i).Cells(42).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT02", tgcGrid.Rows(i).Cells(43).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT03", tgcGrid.Rows(i).Cells(44).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT04", tgcGrid.Rows(i).Cells(45).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT05", tgcGrid.Rows(i).Cells(46).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT06", tgcGrid.Rows(i).Cells(47).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT07", tgcGrid.Rows(i).Cells(48).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT08", tgcGrid.Rows(i).Cells(49).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT09", tgcGrid.Rows(i).Cells(50).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT10", tgcGrid.Rows(i).Cells(51).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT11", tgcGrid.Rows(i).Cells(52).Value.ToString.Trim)
                    comm.Parameters.AddWithValue("@SMT12", tgcGrid.Rows(i).Cells(53).Value.ToString.Trim)
                    comm.ExecuteNonQuery()
                    cn.Close()
                End If
            Next

            MsgBox("Data Berhasil diSimpan sebanyak " + tgcGrid.Rows.Count.ToString + " Data", MsgBoxStyle.Information, "INFORMATION")
        Catch ex As SqlCeException
            MsgBox("ERROR = " + ex.Message, MsgBoxStyle.Critical, "ERROR")
        Catch exx As Exception
            MsgBox("ERROR EXCEPTION = " + exx.Message, MsgBoxStyle.Critical, "ERROR")
        End Try
    End Sub
   

    Public Sub setDGVListFile()
        dgv_listFile.Rows.Add()
        Dim rowsCount As Integer = 0
        rowsCount += dgv_listFile.Rows.Count - 2
        dgv_listFile.Rows(rowsCount).Cells(0).Value = nama_file
        dgv_listFile.Rows(rowsCount).Cells(1).Value = code_cust_list
        dgv_listFile.Rows(rowsCount).Cells(2).Value = cust_name_list
    End Sub

    Public Sub setHeaderDGVListFile()
        dgv_listFile.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        dgv_listFile.Columns.Add("Nama_File", "Nama File")
        dgv_listFile.Columns.Add("cust_code", "Customer Code")
        dgv_listFile.Columns.Add("cust_name", "Nama CUstomer")
    End Sub

    Public Sub cekISUpload(ByVal custCode As String, ByVal tahun As String)
        If cn.State = ConnectionState.Open Then
            cn.Close()
        End If
        cn.Open()

        Dim dtSet As DataSet = New DataSet
        Dim dtSetArea As DataSet = New DataSet
        Dim dt As New DataTable
        Dim dtArea As New DataTable
        Dim dtAdapter As New SqlDataAdapter

        dtAdapter = New SqlDataAdapter("SELECT * FROM TGCM2018 Where CUST_CODE = '" + custCode + "' and tahun = " + tahun, cn)

        dt = Nothing
        dtAdapter.Fill(dtSet)
        dt = dtSet.Tables(0)
        If dt.Rows.Count > 0 Then
            deleteRow(custCode, tahun)
        End If
        cn.Close()

    End Sub

    Public Sub deleteRow(ByVal custCode As String, ByVal tahun As String)
        Dim sqlText As String = "DELETE FROM TGCM2018 WHERE CUST_CODE = @custcode and tahun = @tahun"
        Using cn
            Dim cmd As New SqlCommand(sqlText, cn)
            cmd.Parameters.AddWithValue("@custcode", custCode)
            cmd.Parameters.AddWithValue("@tahun", tahun)
            If cn.State = ConnectionState.Open Then
                cn.Close()                
            End If
            cn.Open()
            cmd.ExecuteNonQuery()
            cn.Close()
        End Using
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles btn_expostcsv.Click
        Dim exprt As New FormExport
        exprt.Show()

    End Sub
End Class
