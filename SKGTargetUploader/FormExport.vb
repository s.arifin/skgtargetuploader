﻿Imports System.Data.SqlClient

Public Class FormExport

    Dim cn As SqlConnection
    Dim StrConn As String

    Public Sub ConnectToSQLServer()
        Try
            StrConn = "Data Source= WIN-COG4TSLIIGA\SERVERSKG;Initial Catalog= dtSKGServer ;User ID= sa;Password= Mis2003;MultipleActiveResultSets=true"
            'StrConn = "Data Source= localhost;Initial Catalog= dtSKGServer ;User ID= sa;Password= sinde1#;MultipleActiveResultSets=true"

            cn = New SqlConnection(StrConn)
            If cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Open()
            End If

        Catch ex As Exception
            MsgBox("[ " & Err.Number.ToString & "  ] " & ex.Message, MsgBoxStyle.Critical, "Error Contact your Administrator")
            cn.Dispose()
        End Try
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_export.CellContentClick

    End Sub

    Private Sub btn_export_Click(sender As Object, e As EventArgs) Handles btn_export.Click
        Dim dlg As New SaveFileDialog
        dlg.Filter = "DAT files (*.DAT)|*.dat|CSV files (*.CSV)|*.csv|All files (*.*)|*.*"
        Dim namaFile As String
        If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
            namaFile = dlg.FileName
            exportCSV(namaFile)
        End If
    End Sub


    Public Sub exportCSV(ByVal fileNamePath As String)
        Try
            Dim p As New Process
            Dim headers = (From header As DataGridViewColumn In dgv_export.Columns.Cast(Of DataGridViewColumn)() Select header.HeaderText).ToArray
            Dim rows = From row As DataGridViewRow In dgv_export.Rows.Cast(Of DataGridViewRow)() Where Not row.IsNewRow Select Array.ConvertAll(row.Cells.Cast(Of DataGridViewCell).ToArray, Function(c) If(c.Value IsNot Nothing, c.Value.ToString.Trim, ""))
            Using sw As New IO.StreamWriter(fileNamePath, False)
                'sw.WriteLine(String.Join(",", headers))
                For Each r In rows
                    sw.WriteLine(String.Join("|", r))
                Next
            End Using
            'p.StartInfo.FileName = fileNamePath
            'p.StartInfo.Verb = ""
            'p.Start()
        Catch ex As Exception
            MsgBox("Error Saat export data = " + ex.Message, MsgBoxStyle.Critical, "ERROR")
        End Try

    End Sub

    Private Sub FormExport_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Public Sub getDataTGCM()
        Try
            ConnectToSQLServer()

            Dim dtSet As DataSet = New DataSet
            Dim dtSetArea As DataSet = New DataSet
            Dim dt As New DataTable
            Dim dtArea As New DataTable
            Dim dtAdapter As New SqlDataAdapter

            'dtAdapter = New SqlDataAdapter("select * from tgcm2018 where tahun = 2020 order by cust_name", cn)
            dtAdapter = New SqlDataAdapter("select * from tgcm2018 where tahun = 2020 and cust_code in ('00816','00825','00812','00867','00819','00815','00824','00810','00820','00833','01240','00798','00836','00811','00829','00827') order by cust_name", cn)

            dt = Nothing
            dtAdapter.Fill(dtSet)
            dt = dtSet.Tables(0)
            dgv_export.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
            dgv_export.DataSource = dt
            count.Text = dt.Rows.Count
            cn.Close()
        Catch ex As Exception
            MsgBox("ERROR saat ambil data = " + ex.Message, MsgBoxStyle.Critical, "ERROR")
            btn_tampil.Enabled = True
        End Try

    End Sub

    Private Sub btn_keluar_Click(sender As Object, e As EventArgs) Handles btn_keluar.Click
        Me.Close()

    End Sub

    Private Sub btn_tampil_Click(sender As Object, e As EventArgs) Handles btn_tampil.Click
        btn_tampil.Enabled = False
        getDataTGCM()
        btn_tampil.Enabled = True
    End Sub
End Class